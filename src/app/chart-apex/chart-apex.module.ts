import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ChartApexComponent} from './chart-apex.component';
import {NgApexchartsModule} from 'ng-apexcharts';

const routes: Routes = [
  {
    path: '',
    component: ChartApexComponent
  }
];

@NgModule({
  declarations: [ChartApexComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgApexchartsModule
  ],
  exports: [ChartApexComponent]
})
export class ChartApexModule { }
