import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {
  ApexDataLabels,
  ChartComponent,
  ApexPlotOptions,
  ApexStates,
  ApexTooltip,
  ApexLegend,
  ApexStroke,
  ApexGrid,
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexChart
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  states: ApexStates;
  tooltip: ApexTooltip;
  legend: ApexLegend;
  stroke: ApexStroke;
  grid: ApexGrid;
};

@Component({
  selector: 'app-chart-apex',
  templateUrl: './chart-apex.component.html',
  styleUrls: ['./chart-apex.component.scss']
})
export class ChartApexComponent implements OnInit {

  constructor() {
  }

  @ViewChild('chart') chart: ChartComponent;
  @Input() values: ApexNonAxisChartSeries = [];
  @Input() colors: string[] = [];
  public chartOptions: Partial<ChartOptions>;
  total: number;

  static thousandSeparators(total) {
    let numReturn = '';
    const numParts = total.toString().split('.');
    numParts[0] = numParts[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    if (!numParts[1]) {
      numReturn = numParts.join('.') + ',00';
    } else {
      numReturn = numParts.join(',');
    }
    return numReturn;
  }

  checkEmptyValues() {
    return this.values.reduce((a, b) => a + b) === 0;
  }

  ngOnInit(): void {
    this.chartOptions = {
      series: this.values.length > 0 && !this.checkEmptyValues() ? this.values : [0.01],
      chart: {
        type: 'donut',
        width: 300,
        height: 300,
        sparkline: {
          enabled: true
        },
        redrawOnParentResize: true,
        redrawOnWindowResize: true
      },
      stroke: {
        width: 0
      },
      grid: {
        padding: {
          top: 15,
          right: 15,
          bottom: 15,
          left: 15
        }
      },
      plotOptions: {
        pie: {
          startAngle: 0,
          endAngle: 360,
          expandOnClick: false,
          offsetX: 0,
          offsetY: 0,
          customScale: 0.6,
          dataLabels: {
            offset: 35,
            minAngleToShowLabel: 10
          },
          donut: {
            size: '80',
            labels: {
              show: true,
              name: {
                offsetY: 30,
                show: true
              },
              value: {
                offsetY: -20,
                fontSize: '32px',
                fontWeight: 'bold',
                show: true
              },
              total: {
                show: true,
                showAlways: true,
                fontSize: '22px',
                fontFamily: 'Arial, verdana, tahoma',
                fontWeight: 'normal',
                label: 'Totale',
                color: 'black',
                formatter: (value) => {
                  if (this.values.length === 0 || this.checkEmptyValues()) {
                    this.total = 0.00;
                  } else if (value.globals.series) {
                    this.total = value.globals.series.reduce((a, b) => a + b, 0);
                  }

                  return ChartApexComponent.thousandSeparators(this.total) + ' €';
                }
              }
            }
          }
        }
      },
      dataLabels: {
        enabled: this.values.length !== 0 && !this.checkEmptyValues(),
        enabledOnSeries: undefined,
        formatter: (val, opts) => {
          return ChartApexComponent.thousandSeparators(opts.w.config.series[opts.seriesIndex]);
        },
        textAnchor: 'middle',
        distributed: false,
        style: {
          fontSize: '14px',
          fontFamily: 'Helvetica, Arial, sans-serif',
          fontWeight: 'bold',
          colors: undefined
        },
        background: {
          enabled: false,
        },
        dropShadow: {
          enabled: false,
        }
      },
      states: {
        hover: {
          filter: {
            type: 'none',
            value: 0,
          }
        }
      },
      tooltip: {
        enabled: false,
      }
    };
  }
}
